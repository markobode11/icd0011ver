package flyway;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class V3_3__create_joining_entries extends BaseJavaMigration {

    @Override
    public void migrate(Context context) {

        var template = new JdbcTemplate(
                new SingleConnectionDataSource(context.getConnection(), true));

        String selectQuery = "SELECT person_id, id AS phone_id " +
                             "  FROM phone";

        String insertQuery = "INSERT INTO person_phone " +
                             "  (person_id, phone_id)" +
                             "  VALUES (?, ?)";

        template.query(selectQuery, x -> {
            Object person_id = x.getObject("person_id");
            Object phone_id = x.getObject("phone_id");

            template.update(insertQuery, person_id, phone_id);
        });
    }
}